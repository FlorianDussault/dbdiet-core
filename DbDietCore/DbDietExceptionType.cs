﻿namespace DbDietCore
{
    public enum  DbDietExceptionType
    {
        Unknown,
        OptionsIsNull,
        OptionsDatabasePathNullOrEmpty,
        OptionsSupportedObjectsIsNullOrEmpty,
        WrongObjectDbDietTableMissing,
        WrongObjectIDbDietObjectMissing,
        WrongObjectFilenameNullOrEmpty,
        WrongObjectFilenameInvalid,
        DatabaseDirectoryNotFound,
        DbNotInitialized,
        DbNotStarted,
        DbAlreadyStarted,
        ObjectNotRegistered,
        ObjectIsNull,
        WrongObjectNotSerializable,
        FileErrorLoad,
        ObjectNotExists,
        WrongObjectNull,
        OptionsOnlyForObjects,
        EncryptWrongParameters,
        Internal
    }
}