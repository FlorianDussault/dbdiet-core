﻿namespace DbDietCore
{
    /// <summary>
    /// Extensions
    /// </summary>
    public static class DbDietExtensions
    {
        /// <summary>
        /// Insert object
        /// </summary>
        /// <param name="dbDietObject">Object</param>
        public static void Insert(this IDbDietObject dbDietObject) => DbDiet.Insert(dbDietObject);

        /// <summary>
        /// Update object
        /// </summary>
        /// <param name="dbDietObject">Object</param>
        public static void Update(this IDbDietObject dbDietObject) => DbDiet.Update(dbDietObject);

        /// <summary>
        /// Delete object
        /// </summary>
        /// <param name="dbDietObject"></param>
        public static void Delete(this IDbDietObject dbDietObject) => DbDiet.Delete(dbDietObject);
    }
}