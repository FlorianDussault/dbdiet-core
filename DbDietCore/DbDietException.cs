﻿using System;

namespace DbDietCore
{
    /// <summary>
    /// DbDiet Exception
    /// </summary>
    public sealed class DbDietException : Exception
    {
        public DbDietExceptionType DbDietExceptionType { get; private set; }
        private Type _type;
        private string _message;
        public override string Message => GetMessage();

        public DbDietException() : base()
        {
            DbDietExceptionType = DbDietExceptionType.Unknown;
        }

        public DbDietException(DbDietExceptionType dietExceptionType = DbDietExceptionType.Unknown, Type type = null, string message = null)
        {
            DbDietExceptionType = dietExceptionType;
            _type = type;
            _message = null;
        }

        private string GetMessage()
        {
            string p = $"ERROR N°{(int)DbDietExceptionType}: ";
            switch (DbDietExceptionType)
            {
                case DbDietExceptionType.Unknown:
                    return $"{p}Unknown error";
                case DbDietExceptionType.OptionsIsNull:
                    return $"{p}Options is null";
                case DbDietExceptionType.OptionsDatabasePathNullOrEmpty:
                    return $"{p}Property '{nameof(DbDietOptions.DatabasePath)}' is null in options";
                case DbDietExceptionType.OptionsSupportedObjectsIsNullOrEmpty:
                    return $"{p}Property '{nameof(DbDietOptions.SupportedObjects)}' is null in options";
                case DbDietExceptionType.WrongObjectDbDietTableMissing:
                    return $"{p}Object {_type}, {nameof(DbDietTableAttribute)} attribute missing";
                case DbDietExceptionType.WrongObjectIDbDietObjectMissing:
                    return $"{p}Object {_type}, {nameof(IDbDietObject)} interface missing";
                case DbDietExceptionType.WrongObjectFilenameNullOrEmpty:
                    return $"{p}Object {_type}, {nameof(DbDietTableAttribute.FileName)} is null or empty";
                case DbDietExceptionType.WrongObjectFilenameInvalid:
                    return $"{p}Object {_type}, {nameof(DbDietTableAttribute.FileName)} is invalid";
                case DbDietExceptionType.DatabaseDirectoryNotFound:
                    return $"{p}Database directory not found";
                case DbDietExceptionType.DbNotInitialized:
                    return $"{p}Database not initialized";
                case DbDietExceptionType.DbNotStarted:
                    return $"{p}Start engine before";
                case DbDietExceptionType.DbAlreadyStarted:
                    return $"{p}Engine already started";
                case DbDietExceptionType.ObjectNotRegistered:
                    return $"{p}Object {_type} not registered";
                case DbDietExceptionType.ObjectIsNull:
                    return $"{p}Object is null";
                case DbDietExceptionType.WrongObjectNotSerializable:
                    return $"{p}Object {_type} not serializable";
                case DbDietExceptionType.FileErrorLoad:
                    return $"{p}Error to load file {_message}";
                case DbDietExceptionType.ObjectNotExists:
                case DbDietExceptionType.Internal:
                    return $"{p}{_message}";
                case DbDietExceptionType.WrongObjectNull:
                    return $"{p}Object type null";
                case DbDietExceptionType.OptionsOnlyForObjects:
                    return $"{p}Option {_message} is only for objects ({nameof(DbDietTableAttribute)})";
                case DbDietExceptionType.EncryptWrongParameters:
                    return $"{p}{nameof(DbDietOptions.EncryptKey)} or {nameof(DbDietOptions.EncryptIV)} is null or empty";
                default:
                    return "Unknown error";
            }
        }
    }
}