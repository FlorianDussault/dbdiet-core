﻿namespace DbDietCore
{
    public interface IDbDietObject
    {
        uint DietId { get; set; }
    }
}