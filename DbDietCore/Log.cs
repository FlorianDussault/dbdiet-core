﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Threading;
using Microsoft.Extensions.Logging;

namespace DbDietCore
{
    internal class Log
    {
        private static Log _instance;
        private static Log Instance => _instance ?? (_instance = new Log());

        private ILogger _logger;

        private Dictionary<int, int> _increments = new Dictionary<int, int>();

        private bool _isStart = false;
        private bool IsRunning => _logger != null && _isStart;

        private Log()
        {

        }

        internal static void SetLogger(ILogger logger, bool start = true)
        {
            Instance._logger = logger;
            Instance._isStart = true;
            LogInformation("Logger initialized");

        }

        internal static void Start()
        {
            Instance._isStart = true;
        }
        internal static void Stop()
        {
            Instance._isStart = false;
        }

        internal static void LogTrace(string message)
        {
            if (Instance.IsRunning)
                Instance._logger.LogTrace(FormatMessage(message));
        }

        internal static T LogTraceAndReturn<T>(T value, string message)
        {
            if (Instance.IsRunning)
                Instance._logger.LogTrace(FormatMessage($"{message} [return:{value?.ToString() ?? "null"}]"));
            return value;
        }

        internal static void LogInformation(string message)
        {
            if (Instance.IsRunning)
                Instance._logger.LogInformation(FormatMessage(message));
        }

        private static string FormatMessage(string message)
        {
            int threadId = Thread.CurrentThread.ManagedThreadId;

            if (!Instance._increments.ContainsKey(threadId)) Instance._increments.Add(threadId, 0);

            if (message.StartsWith("<<"))
                Instance._increments[threadId]--;

            string spaces = string.Empty;
            for (int i = 0; i < Instance._increments[threadId]; i++)
                spaces += " ";

            if (message.StartsWith(">>"))
                Instance._increments[threadId]++;

            return $"[{Process.GetCurrentProcess().Id}]\t[{Thread.CurrentThread.ManagedThreadId}]\t{spaces}{message}";
        }
    }
}
