﻿using System;
using Microsoft.Extensions.Logging;

namespace DbDietCore
{
    /// <summary>
    /// Options
    /// </summary>
    public class DbDietOptions
    {
        /// <summary>
        /// Database path
        /// </summary>
        public string DatabasePath { get; set; }

        /// <summary>
        /// DbDiet will build the database directory (if needed)
        /// </summary>
        public bool BuildDatabaseDirectory { get; set; }

        /// <summary>
        /// Supported object
        /// </summary>
        public Type [] SupportedObjects { get; set; }

        /// <summary>
        /// Split limit
        /// </summary>
        public uint SplitLimit { get; set; }

        /// <summary>
        /// Split options
        /// </summary>
        public DbDietSplitOptions SplitOptions { get; set; } = DbDietSplitOptions.NoSplit;

        /// <summary>
        /// Encrypt options
        /// </summary>
        public DbDietEncryptOptions EncryptOptions { get; set; } = DbDietEncryptOptions.NoEncrypt;

        /// <summary>
        /// Encryption Key
        /// </summary>
        public byte[] EncryptKey { get; set; }

        /// <summary>
        /// Encryption IV
        /// </summary>
        public byte[] EncryptIV { get; set; }

        /// <summary>
        /// Database instance name
        /// </summary>
        public string InstanceName { get; set; }

        /// <summary>
        /// Used by an unique process
        /// </summary>
        public bool RunningOnUniqueInstance { get; set; } = true;
        /// <summary>
        /// Logger
        /// </summary>
        public ILogger Logger { get; set; }

    }

}