﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace DbDietCore
{
    /// <summary>
    /// DbDiet !
    /// </summary>
    public sealed class DbDiet
    {
        /// <summary>
        /// Filename extension
        /// </summary>
        private const string FILENAME_EXT = "diet";

        /// <summary>
        /// Separator for indexes
        /// </summary>
        private const string INDEX_SEPARATOR = "@IV@";

        /// <summary>
        /// Separator for indexes values
        /// </summary>
        private const string INDEX_SEPARATOR_KEY_VALUE = "@KV@";

        /// <summary>
        /// Singleton instance
        /// </summary>
        private static DbDiet _instance;

        /// <summary>
        /// Class attributes of Diet objects
        /// </summary>
        private readonly Dictionary<Type, DbDietTableAttribute> _dbDietTableAttributes;

        /// <summary>
        /// Mutex to protect (for threads)
        /// </summary>
        private Mutex _mutex = null;

        /// <summary>
        /// Objects cache
        /// </summary>
        private readonly Dictionary<Type, Dictionary<uint, IDbDietObject>> _objects;

        private string DbDietIndexesPath => Path.Combine(_options.DatabasePath, "dbdiet.indexes");

        private readonly Dictionary<Type, Guid> _objectVersions;

        /// <summary>
        /// Flag if DBDiet is running
        /// </summary>
        private bool _dbRunning;

        /// <summary>
        /// Options
        /// </summary>
        private DbDietOptions _options;

        /// <summary>
        /// Private constructor
        /// </summary>
        private DbDiet()
        {
           _objects = new Dictionary<Type, Dictionary<uint, IDbDietObject>>();
            _dbDietTableAttributes = new Dictionary<Type, DbDietTableAttribute>();
            _objectVersions = new Dictionary<Type, Guid>();
        }

        /// <summary>
        /// Access to singleton instance
        /// </summary>
        private static DbDiet Instance => _instance ?? (_instance = new DbDiet());

        /// <summary>
        /// Check if singleton was created
        /// </summary>
        public static bool HasInstance => _instance == null;

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="dbDietOptions">Options</param>
        public static void Initialize(DbDietOptions dbDietOptions)
        {
            Log.SetLogger(dbDietOptions.Logger, false);
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(Initialize)}");

            Instance.ChechDbRuning(false);

            #region Check options

            if (dbDietOptions == null)
                throw new DbDietException(DbDietExceptionType.OptionsIsNull);
            if (dbDietOptions.SupportedObjects == null || !dbDietOptions.SupportedObjects.Any())
                throw new DbDietException(DbDietExceptionType.OptionsSupportedObjectsIsNullOrEmpty);
            if (string.IsNullOrWhiteSpace(dbDietOptions.DatabasePath))
                throw new DbDietException(DbDietExceptionType.OptionsDatabasePathNullOrEmpty);
            if (dbDietOptions.EncryptOptions == DbDietEncryptOptions.ReferToOptions)
                throw new DbDietException(DbDietExceptionType.OptionsOnlyForObjects, null,
                    nameof(DbDietEncryptOptions.ReferToOptions));
            if (dbDietOptions.SplitOptions == DbDietSplitOptions.ReferToOptions)
                throw new DbDietException(DbDietExceptionType.OptionsOnlyForObjects, null,
                    nameof(DbDietSplitOptions.ReferToOptions));
            if (dbDietOptions.EncryptOptions == DbDietEncryptOptions.Encrypt &&
                (dbDietOptions.EncryptKey == null || dbDietOptions.EncryptKey.Length == 0 ||
                 dbDietOptions.EncryptIV == null || dbDietOptions.EncryptIV.Length == 0))
                throw new DbDietException(DbDietExceptionType.EncryptWrongParameters);

            #endregion

            #region Check supported types

            foreach (Type type in dbDietOptions.SupportedObjects)
            {
                if (type == null)
                    throw new DbDietException(DbDietExceptionType.WrongObjectNull);
                if (type.GetInterfaces().All(i => i != typeof(IDbDietObject)))
                    throw new DbDietException(DbDietExceptionType.WrongObjectIDbDietObjectMissing, type);
                if (!type.GetCustomAttributes<SerializableAttribute>().Any())
                    throw new DbDietException(DbDietExceptionType.WrongObjectNotSerializable, type);
                if (!type.GetCustomAttributes(typeof(DbDietTableAttribute)).Any())
                    throw new DbDietException(DbDietExceptionType.WrongObjectDbDietTableMissing, type);
                DbDietTableAttribute dbDietTableAttribute =
                    (DbDietTableAttribute) type.GetCustomAttributes(typeof(DbDietTableAttribute)).First();
                if (string.IsNullOrEmpty(dbDietTableAttribute.FileName))
                    throw new DbDietException(DbDietExceptionType.WrongObjectFilenameNullOrEmpty, type);
                if (dbDietTableAttribute.FileName.IndexOfAny(Path.GetInvalidFileNameChars()) >= 0)
                    throw new DbDietException(DbDietExceptionType.WrongObjectFilenameInvalid, type);
                if (dbDietTableAttribute.EncryptOptions == DbDietEncryptOptions.Encrypt)
                {
                    if (dbDietOptions.EncryptKey == null || dbDietOptions.EncryptKey.Length == 0)
                        throw new DbDietException(DbDietExceptionType.EncryptWrongParameters);
                    if (dbDietOptions.EncryptIV == null || dbDietOptions.EncryptIV.Length == 0)
                        throw new DbDietException(DbDietExceptionType.EncryptWrongParameters);
                }
            }

            #endregion

            Instance._options = dbDietOptions;
            Log.LogTrace($"<< {nameof(DbDiet)}::{nameof(Initialize)}");
        }

        /// <summary>
        /// Reset DbDiet
        /// </summary>
        public static void Reset()
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(Reset)}");
            _instance = null;
            Log.LogTrace($"<< {nameof(DbDiet)}::{nameof(Reset)}");

        }

        /// <summary>
        /// Check engine status
        /// </summary>
        /// <param name="value">Check value</param>
        private void ChechDbRuning(bool value)
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(ChechDbRuning)}");
            if (value && !_dbRunning)
                throw new DbDietException(DbDietExceptionType.DbNotStarted);
            if (!value && _dbRunning)
                throw new DbDietException(DbDietExceptionType.DbAlreadyStarted);
            Log.LogTrace($"<< {nameof(DbDiet)}::{nameof(ChechDbRuning)}");

        }

        /// <summary>
        /// Start engine !
        /// </summary>
        public static void Start()
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(Start)}");
            if (Instance._options == null)
                throw new DbDietException(DbDietExceptionType.DbNotInitialized);
            Instance.ChechDbRuning(false);
            Instance.InternalStart();
            Log.LogTrace($"<< {nameof(DbDiet)}::{nameof(Start)}");
        }

        /// <summary>
        /// Start engine ! (internal)
        /// </summary>
        private void InternalStart()
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(InternalStart)}");
            foreach (Type type in _options.SupportedObjects)
            {
                _objects.Add(type, new Dictionary<uint, IDbDietObject>());
                _dbDietTableAttributes.Add(type,
                    (DbDietTableAttribute)type.GetCustomAttributes(typeof(DbDietTableAttribute)).First());
                _objectVersions.Add(type, Guid.Empty);
            }

            string instanceName = string.IsNullOrEmpty(_options.InstanceName) ? "__DEFAULT__" : _options.InstanceName;
            _mutex = new Mutex(false, $"Global\\{instanceName}.DBDIET.MUTEX");

            MutexProtection(() =>
            {
                if (!Directory.Exists(_options.DatabasePath))
                {
                    if (!_options.BuildDatabaseDirectory)
                        throw new DbDietException(DbDietExceptionType.DatabaseDirectoryNotFound);
                    Directory.CreateDirectory(_options.DatabasePath);
                }

                if (!File.Exists(DbDietIndexesPath))
                {
                    foreach (Type type in _options.SupportedObjects)
                    {
                        UpdateIndexesFile(type);
                    }
                }

                foreach (Type type in _options.SupportedObjects) Load(type);
            });
            
            _dbRunning = true;
            Log.LogTrace($"<< {nameof(DbDiet)}::{nameof(InternalStart)}");
        }

        #region Indexes

        private Dictionary<string, Guid> GetIndexes()
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(GetIndexes)}");
            Dictionary<string, Guid> indexes = new Dictionary<string, Guid>();
            if (File.Exists(DbDietIndexesPath))
            {
                string chain = File.ReadAllText(DbDietIndexesPath);
                if (!string.IsNullOrEmpty(chain))
                {
                    foreach (string keyValue in chain.Split(INDEX_SEPARATOR, StringSplitOptions.RemoveEmptyEntries))
                    {
                        string[] splited = keyValue.Split(INDEX_SEPARATOR_KEY_VALUE, StringSplitOptions.RemoveEmptyEntries);
                        indexes.Add(splited[0], Guid.Parse(splited[1]));
                    }
                }
            }
            return Log.LogTraceAndReturn(indexes, $"<< {nameof(DbDiet)}::{nameof(GetIndexes)}");
        }

        /// <summary>
        /// Update index files
        /// </summary>
        private void UpdateIndexesFile(Type type)
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(UpdateIndexesFile)}");

            Dictionary<string, Guid> indexes = GetIndexes();

            if (!indexes.ContainsKey(type.FullName))
                indexes.Add(type.FullName, _objectVersions[type]);
            else
                indexes[type.FullName] = _objectVersions[type];

            StringBuilder sb = new StringBuilder();
            foreach (KeyValuePair<string, Guid> keyValuePair in indexes)
                sb.Append($"{keyValuePair.Key}{INDEX_SEPARATOR_KEY_VALUE}{keyValuePair.Value}{INDEX_SEPARATOR}");
            
            File.WriteAllText(DbDietIndexesPath, sb.ToString());
            Log.LogTrace($"<< {nameof(DbDiet)}::{nameof(UpdateIndexesFile)}");
        }

        /// <summary>
        /// Get index value from type
        /// </summary>
        /// <param name="type">Type</param>
        /// <returns></returns>
        private Guid GetIndexValueForType(Type type)
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(GetIndexValueForType)}");
            Dictionary<string, Guid> indexes = GetIndexes();
            if (!indexes.ContainsKey(type.FullName))
                throw new DbDietException(DbDietExceptionType.Internal, type, $"Internal error: unknown type '{type.FullName}' in indexes");
            return Log.LogTraceAndReturn(indexes[type.FullName], $"<< {nameof(DbDiet)}::{nameof(GetIndexValueForType)}");
        }

        /// <summary>
        /// Reload object from file if index is obsolete
        /// </summary>
        /// <param name="type"></param>
        private void ReloadIfIndexObsolete(Type type)
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(ReloadIfIndexObsolete)}");
            Guid indexValue = GetIndexValueForType(type);
            if (_objectVersions[type] == indexValue)
            {
                Log.LogTrace($"Check version for type {type.Name} => Same ({indexValue})");
                Log.LogTrace($"<< {nameof(DbDiet)}::{nameof(ReloadIfIndexObsolete)}");
                return;
            }
            Log.LogTrace($"Check version for type {type.Name} => Different (in memory: {_objectVersions[type]}, in file: {indexValue})");
            Load(type);
            _objectVersions[type] = indexValue;
            Log.LogTrace($"<< {nameof(DbDiet)}::{nameof(ReloadIfIndexObsolete)}");
        }

        #endregion

        #region Mutex

        /// <summary>
        /// Call function (return value) protected by mutex
        /// </summary>
        /// <typeparam name="T">Type of returned value</typeparam>
        /// <param name="function">Function called</param>
        /// <returns>Value returned</returns>
        private T MutexProtection<T>(Func<T> function)
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(MutexProtection)}");
            T ret;
            _mutex.WaitOne();
            try
            {
                ret = function();
                _mutex.ReleaseMutex();
            }
            catch (Exception)
            {
                _mutex.ReleaseMutex();
                throw;
            }
            return Log.LogTraceAndReturn(ret, $"<< {nameof(DbDiet)}::{nameof(MutexProtection)}");
        }

        /// <summary>
        /// Call function
        /// </summary>
        /// <param name="action">Function</param>
        private void MutexProtection(Action action)
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(MutexProtection)}");
            _mutex.WaitOne();
            try
            {
                action();
                _mutex.ReleaseMutex();
            }
            catch (Exception)
            {
                _mutex.ReleaseMutex();
                throw;
            }
            Log.LogTrace($"<< {nameof(DbDiet)}::{nameof(MutexProtection)}");
        }

        #endregion

        #region Insert

        /// <summary>
        /// Insert object
        /// </summary>
        /// <typeparam name="T">Type of object to insert</typeparam>
        /// <param name="dbDietObject">Object to insert</param>
        public static void Insert<T>(T dbDietObject) where T : IDbDietObject
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(Insert)}<{typeof(T).Name}>");

            Instance.ChechDbRuning(true);
            if (dbDietObject == null)
                throw new DbDietException(DbDietExceptionType.ObjectIsNull);

            Instance.InternalInsert(dbDietObject);
            Log.LogTrace($"<< {nameof(DbDiet)}::{nameof(Insert)}<{typeof(T).Name}>");

        }

        /// <summary>
        /// Insert object (internal)
        /// </summary>
        /// <param name="dbDietObject">Object to insert</param>
        private void InternalInsert(IDbDietObject dbDietObject)
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(InternalInsert)}");
            MutexProtection(() =>
            {
                ReloadIfIndexObsolete(dbDietObject.GetType());
                if (!_objects.ContainsKey(dbDietObject.GetType()))
                    throw new DbDietException(DbDietExceptionType.ObjectNotRegistered, dbDietObject.GetType());
                if (_objects[dbDietObject.GetType()].Keys.Count > 0)
                    dbDietObject.DietId = _objects[dbDietObject.GetType()].Keys.Max() + 1;
                else
                    dbDietObject.DietId = 1;

                IDbDietObject clone = Clone(dbDietObject);
                _objects[dbDietObject.GetType()].Add(dbDietObject.DietId, clone);
                Save(dbDietObject.GetType(), dbDietObject.DietId);
            });
            Log.LogTrace($"<< {nameof(DbDiet)}::{nameof(InternalInsert)}");
        }

        #endregion

        #region Get

        /// <summary>
        /// Get object
        /// </summary>
        /// <typeparam name="T">Type of object</typeparam>
        /// <param name="id">Object ID to find</param>
        /// <returns>Object (or null if not found)</returns>
        public static T Get<T>(uint id) where T : IDbDietObject
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(Get)}<{typeof(T).Name}>");
            Instance.ChechDbRuning(true);
            return Log.LogTraceAndReturn(Instance.InternalGet<T>(id), $"<< {nameof(DbDiet)}::{nameof(Get)}<{typeof(T).Name}>");
        }

        /// <summary>
        /// Get object (internal)
        /// </summary>
        /// <typeparam name="T">Type of object</typeparam>
        /// <param name="id">Object ID to find</param>
        /// <returns>Object (or null if not found)</returns>
        private T InternalGet<T>(uint id) where T : IDbDietObject
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(Get)}<{typeof(T).Name}>");

            T obj = MutexProtection(() =>
            {
                ReloadIfIndexObsolete(typeof(T));

                if (!_objects.ContainsKey(typeof(T)))
                    throw new DbDietException(DbDietExceptionType.ObjectNotRegistered, typeof(T));
                T ret = default;
                if (_objects[typeof(T)].ContainsKey(id))
                    ret = (T) Clone(_objects[typeof(T)][id]);

                return ret;
            });
            return Log.LogTraceAndReturn(obj, $"<< {nameof(DbDiet)}::{nameof(Get)}<{typeof(T).Name}>");
        }

        /// <summary>
        /// Get all object
        /// </summary>
        /// <typeparam name="T">Type of object</typeparam>
        /// <returns>List of objects</returns>
        public static List<T> Get<T>() where T : IDbDietObject
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(Get)}<{typeof(T).Name}>");
            Instance.ChechDbRuning(true);
            return Log.LogTraceAndReturn(Instance.GetInternal<T>(), $"<< {nameof(DbDiet)}::{nameof(Get)}<{typeof(T).Name}>");
        }

        /// <summary>
        /// Get all object (internal)
        /// </summary>
        /// <typeparam name="T">Type of object</typeparam>
        /// <returns>List of objects</returns>
        private List<T> GetInternal<T>() where T : IDbDietObject
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(GetInternal)}<{typeof(T).Name}>");
            return Log.LogTraceAndReturn(MutexProtection(() =>
            {
                ReloadIfIndexObsolete(typeof(T));
                if (!_objects.ContainsKey(typeof(T)))
                    throw new DbDietException(DbDietExceptionType.ObjectNotRegistered, typeof(T));
                List<T> objects = new List<T>();
                foreach (IDbDietObject dbDietObject in _objects[typeof(T)].Values)
                    objects.Add((T) Clone(dbDietObject));
                return objects;
            }), $"<< {nameof(DbDiet)}::{nameof(Get)}<{typeof(T).Name}>");
        }

        #endregion

        #region Update

        /// <summary>
        /// Update object
        /// </summary>
        /// <typeparam name="T">Type of object</typeparam>
        /// <param name="dbDietObject">Object to update</param>
        public static void Update<T>(T dbDietObject) where T : IDbDietObject
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(Update)}<{typeof(T).Name}>");
            Instance.ChechDbRuning(true);
            if (dbDietObject == null)
                throw new DbDietException(DbDietExceptionType.ObjectIsNull);
            Instance.InternalUpdate(dbDietObject);
            Log.LogTrace($"<< {nameof(DbDiet)}::{nameof(Update)}<{typeof(T).Name}>");
        }

        /// <summary>
        /// Update object (internal)
        /// </summary>
        /// <typeparam name="T">Type of object</typeparam>
        /// <param name="dbDietObject">Object to update</param>
        private void InternalUpdate<T>(T dbDietObject) where T : IDbDietObject
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(InternalUpdate)}<{typeof(T).Name}>");
            MutexProtection(() =>
            {
                ReloadIfIndexObsolete(dbDietObject.GetType());
                if (!_objects.ContainsKey(dbDietObject.GetType()))
                    throw new DbDietException(DbDietExceptionType.ObjectNotRegistered, dbDietObject.GetType());
                if (!_objects[dbDietObject.GetType()].ContainsKey(dbDietObject.DietId))
                    throw new DbDietException(DbDietExceptionType.ObjectNotExists, dbDietObject.GetType(),
                        $"Can't update, object not exists with id {dbDietObject.DietId}");
                _objects[dbDietObject.GetType()][dbDietObject.DietId] = Clone(dbDietObject);
                Save(dbDietObject.GetType(), dbDietObject.DietId);
            });
            Log.LogTrace($"<< {nameof(DbDiet)}::{nameof(InternalUpdate)}<{typeof(T).Name}>");
        }

        #endregion

        #region Delete

        /// <summary>
        /// Delete object
        /// </summary>
        /// <param name="dbDietObject">Object to delete</param>
        public static void Delete(IDbDietObject dbDietObject)
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(Delete)}");
            Instance.ChechDbRuning(true);
            if (dbDietObject == null)
                throw new DbDietException(DbDietExceptionType.ObjectIsNull);
            Instance.InternalDelete(dbDietObject);
            Log.LogTrace($"<< {nameof(DbDiet)}::{nameof(Delete)}");
        }

        /// <summary>
        /// Delete object (internal)
        /// </summary>
        /// <param name="dbDietObject">Object to deletes</param>
        private void InternalDelete(IDbDietObject dbDietObject)
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(InternalDelete)}");

            MutexProtection(() =>
            {
                ReloadIfIndexObsolete(dbDietObject.GetType());
                if (!_objects.ContainsKey(dbDietObject.GetType()))
                    throw new DbDietException(DbDietExceptionType.ObjectNotRegistered, dbDietObject.GetType());
                if (!_objects[dbDietObject.GetType()].ContainsKey(dbDietObject.DietId))
                    throw new DbDietException(DbDietExceptionType.ObjectNotExists, dbDietObject.GetType(),
                        $"Can't delete, object not exists with id {dbDietObject.DietId}");
                _objects[dbDietObject.GetType()].Remove(dbDietObject.DietId);
                Save(dbDietObject.GetType(), dbDietObject.DietId);
            });
            Log.LogTrace($"<< {nameof(DbDiet)}::{nameof(InternalDelete)}");
        }

        #endregion

        #region Clear

        /// <summary>
        /// Clear (erase) database
        /// </summary>
        public static void ClearDb()
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(ClearDb)}");
            Instance.InternalClearObjects();
            Log.LogTrace($"<< {nameof(DbDiet)}::{nameof(ClearDb)}");
        }

        /// <summary>
        /// Clear all objects for a type
        /// </summary>
        /// <typeparam name="T">Type of object</typeparam>
        public static void ClearObjects<T>()
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(ClearObjects)}<{typeof(T).Name}>");
            Instance.InternalClearObjects(typeof(T));
            Log.LogTrace($"<< {nameof(DbDiet)}::{nameof(ClearObjects)}<{typeof(T).Name}>");
        }

        /// <summary>
        /// Clear all objects for a type (internal)
        /// </summary>
        /// <param name="type">TYpe of object</param>
        private void InternalClearObjects(Type type = null)
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(InternalClearObjects)}");
            MutexProtection(() =>
            {
                ReloadIfIndexObsolete(type);
                List<Type> toRemove = new List<Type>();
                if (type != null)
                    toRemove.Add(type);
                else
                    toRemove.AddRange(_objects.Keys);

                foreach (Type toRemoveType in toRemove)
                {
                    _objects[toRemoveType].Clear();
                    string path = GetTypeFileName(toRemoveType);
                    if (File.Exists(path))
                        File.Delete(path);
                }
            });
            Log.LogTrace($"<< {nameof(DbDiet)}::{nameof(InternalClearObjects)}");
        }

        #endregion

        #region Load & Save

        /// <summary>
        /// Save object
        /// </summary>
        /// <param name="type">Type of object</param>
        /// <param name="id">ID of object</param>
        private void Save(Type type, uint id)
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(Save)}");
            DbDietTableAttribute tableAttribute = _dbDietTableAttributes[type];

            Log.LogInformation($"Save object, type: {type.Name}, id: {id}");

            bool split = IsSplit(tableAttribute);
            string path;
            uint minId, maxId;
            Dictionary<uint, IDbDietObject> tempDictionary = new Dictionary<uint, IDbDietObject>();
            if (split)
            {
                uint index = Convert.ToUInt32(Math.Floor((decimal) id / _options.SplitLimit));
                path = Path.Combine(_options.DatabasePath, $"{tableAttribute.FileName}.{index}.{FILENAME_EXT}");
                minId = index * _options.SplitLimit;
                maxId = minId + _options.SplitLimit - 1;
                if (minId == 0) minId = 1;
                IEnumerable<uint> ids = _objects[type].Keys.Where(kid => kid >= minId && kid <= maxId);
                foreach (uint kid in ids)
                    tempDictionary.Add(kid, _objects[type][kid]);
            }
            else
            {
                path = Path.Combine(_options.DatabasePath, $"{tableAttribute.FileName}.{FILENAME_EXT}");
                tempDictionary = _objects[type];
            }

            _objectVersions[type] = Guid.NewGuid();
            UpdateIndexesFile(type);

            IsEncrypt(type, out DESCryptoServiceProvider cryptoServiceProvider);

            SaveDbObjects(path, tempDictionary, cryptoServiceProvider?.CreateEncryptor());
            Log.LogTrace($"<< {nameof(DbDiet)}::{nameof(Save)}");
        }

        /// <summary>
        /// Save objects
        /// </summary>
        /// <param name="path">File path</param>
        /// <param name="dbDietObjects">Objects</param>
        /// <param name="cryptoTransform">Encrypt object</param>
        private void SaveDbObjects(string path, Dictionary<uint, IDbDietObject> dbDietObjects,
            ICryptoTransform cryptoTransform = null)
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(SaveDbObjects)}");
            List<IDbDietObject> list = dbDietObjects.Values.ToList();

            FileInfo fileInfo = new FileInfo(path);
            if (fileInfo.Exists)
            {
                string newFile = path.Replace(".died", $".old.{Guid.NewGuid()}");
                fileInfo.MoveTo(newFile);
                File.Delete(newFile);
            }

            if (list.Count == 0)
            {
                Log.LogTrace($"<< {nameof(DbDiet)}::{nameof(SaveDbObjects)}");
                return;
            }

            using (Stream streamWriter = File.Create(path))
            {
                IFormatter formatter = new BinaryFormatter();
                if (cryptoTransform != null)
                {
                    using (Stream cryptoStream =
                        new CryptoStream(streamWriter, cryptoTransform, CryptoStreamMode.Write))
                    {
                        formatter.Serialize(cryptoStream, list);
                        cryptoStream.Close();
                    }
                }
                else
                {
                    formatter.Serialize(streamWriter, list);
                }
                streamWriter.Close();
            }
            Log.LogTrace($"<< {nameof(DbDiet)}::{nameof(SaveDbObjects)}");

        }

        /// <summary>
        /// Load objects
        /// </summary>
        /// <param name="type">Type of object</param>
        private void Load(Type type)
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(Load)}");
            DbDietTableAttribute attribute = _dbDietTableAttributes[type];

            List<string> paths = new List<string>();
            _objects[type] = new Dictionary<uint, IDbDietObject>();
            if (IsSplit(type))
            {
                foreach (FileInfo fileInfo in new DirectoryInfo(_options.DatabasePath).GetFiles())
                {
                    Regex regex = new Regex($"{attribute.FileName}\\.([0-9]*)\\.diet");
                    if (regex.IsMatch(fileInfo.Name)) paths.Add(fileInfo.FullName);
                }
            }
            else
            {
                string fileName = GetTypeFileName(type);
                if (!File.Exists(fileName)) return;
                paths.Add(fileName);
            }

            _objectVersions[type] = GetIndexValueForType(type);

            IsEncrypt(type, out DESCryptoServiceProvider cryptoServiceProvider);

            foreach (string path in paths)
            {
                List<IDbDietObject> objects = LoadDbObjects(path, cryptoServiceProvider?.CreateDecryptor());
                foreach (IDbDietObject dbDietObject in objects) _objects[type].Add(dbDietObject.DietId, dbDietObject);
            }
            Log.LogTrace($"<< {nameof(DbDiet)}::{nameof(Load)}");
        }

        /// <summary>
        /// Load objects
        /// </summary>
        /// <param name="path">File path</param>
        /// <param name="cryptoTransform">Encrypt object</param>
        /// <returns></returns>
        public static List<IDbDietObject> LoadDbObjects(string path, ICryptoTransform cryptoTransform = null)
        {
            Log.LogTrace($">> {nameof(DbDiet)}::{nameof(LoadDbObjects)}");
            if (!File.Exists(path))
                return Log.LogTraceAndReturn(new List<IDbDietObject>(), $"<< {nameof(DbDiet)}::{nameof(LoadDbObjects)}");
            List<IDbDietObject> list;
            try
            {
                using (StreamReader streamReader = new StreamReader(path))
                {
                    BinaryFormatter binaryFormatter = new BinaryFormatter();

                    if (cryptoTransform != null)
                        using (Stream cryptoStream = new CryptoStream(streamReader.BaseStream, cryptoTransform,
                            CryptoStreamMode.Read))
                        {
                            list = (List<IDbDietObject>) binaryFormatter.Deserialize(cryptoStream);
                        }
                    else
                        list = (List<IDbDietObject>) binaryFormatter.Deserialize(streamReader.BaseStream);
                }
            }
            catch (Exception ex)
            {
                throw new DbDietException(DbDietExceptionType.FileErrorLoad, null, ex.Message);
            }

            return Log.LogTraceAndReturn(list, $"<< {nameof(DbDiet)}::{nameof(LoadDbObjects)}");
        }

        #endregion

        #region Tools

        /// <summary>
        /// Get file name for an object name
        /// </summary>
        /// <param name="type">Type of object</param>
        /// <returns>File name</returns>
        private string GetTypeFileName(Type type) => $"{Path.Combine(_options.DatabasePath, _dbDietTableAttributes[type].FileName)}.{FILENAME_EXT}";

        /// <summary>
        /// Check if object type is split
        /// </summary>
        /// <param name="type">Object type</param>
        /// <returns>true if object type is split</returns>
        private bool IsSplit(Type type)
        {
            return IsSplit(_dbDietTableAttributes[type]);
        }

        /// <summary>
        /// Check if object type is split
        /// </summary>
        /// <param name="attribute">Object attribute</param>
        /// <returns>true if object type is split</returns>
        private bool IsSplit(DbDietTableAttribute attribute)
        {
            return attribute.SplitOptions == DbDietSplitOptions.Split ||
                   attribute.SplitOptions == DbDietSplitOptions.ReferToOptions &&
                   _options.SplitOptions == DbDietSplitOptions.Split;
        }

        /// <summary>
        /// Check if object is encrypted
        /// </summary>
        /// <param name="type">Object type</param>
        /// <param name="cryptoServiceProvider">Encrypt object</param>
        /// <returns>true if the object is encrypted</returns>
        private bool IsEncrypt(Type type, out DESCryptoServiceProvider cryptoServiceProvider)
        {
            return IsEncrypt(_dbDietTableAttributes[type], out cryptoServiceProvider);
        }

        /// <summary>
        /// Check if object is encrypted
        /// </summary>
        /// <param name="attribute">Attribute object</param>
        /// <param name="cryptoServiceProvider">Encrypt object</param>
        /// <returns>true if the object is encrypted</returns>
        private bool IsEncrypt(DbDietTableAttribute attribute, out DESCryptoServiceProvider cryptoServiceProvider)
        {
            cryptoServiceProvider = null;
            if (attribute.EncryptOptions == DbDietEncryptOptions.Encrypt ||
                attribute.EncryptOptions == DbDietEncryptOptions.ReferToOptions &&
                _options.EncryptOptions == DbDietEncryptOptions.Encrypt)
            {
                cryptoServiceProvider = new DESCryptoServiceProvider
                {
                    Key = _options.EncryptKey,
                    IV = _options.EncryptIV
                };
                return true;
            }

            return false;
        }

        /// <summary>
        /// Clone object
        /// </summary>
        /// <param name="dbDietObject">Object</param>
        /// <returns>Object</returns>
        private static IDbDietObject Clone(IDbDietObject dbDietObject)
        {
            if (dbDietObject is ICloneable cloneable)
                return (IDbDietObject) cloneable.Clone();

            IDbDietObject obj;
            using (MemoryStream stream = new MemoryStream())
            {
                IFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, dbDietObject);
                stream.Seek(0, SeekOrigin.Begin);
                obj = (IDbDietObject) formatter.Deserialize(stream);
            }

            return obj;
        }

        #endregion
    }
}