﻿using System;

namespace DbDietCore
{
    /// <summary>
    /// Object attribute
    /// </summary>
    public class DbDietTableAttribute : Attribute
    {
        /// <summary>
        /// Filename
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Split options
        /// </summary>
        public DbDietSplitOptions SplitOptions { get; set; } = DbDietSplitOptions.ReferToOptions;

        /// <summary>
        /// Encrypt options
        /// </summary>
        public DbDietEncryptOptions EncryptOptions { get; set; } = DbDietEncryptOptions.ReferToOptions;
    }
}