﻿using System;
using System.IO;
using DbDietCore;

namespace DbDietCoreExamples
{

    /// <summary>
    /// Person
    /// </summary>
    [DbDietTable(FileName = nameof(Person), SplitOptions = DbDietSplitOptions.Split)]
    [Serializable]
    public class Person : IDbDietObject
    {
        public uint DietId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }
    }

    internal class Split
    {
        public static void Run()
        {
            // Options
            DbDietOptions options = new DbDietOptions
            {
                // Database path
                DatabasePath = Path.Combine(Path.GetTempPath(), "DBDIETCORE_SPLIT"),
                // Build directory database on start
                BuildDatabaseDirectory = true,
                // No encryption
                EncryptOptions = DbDietEncryptOptions.NoEncrypt,
                // Split option
                SplitLimit = 10,
                // Supported objects
                SupportedObjects = new[] { typeof(Person) }
            };

            // Initialize engine
            DbDiet.Initialize(options);

            // Start engine
            DbDiet.Start();

            // Clear DB
            DbDiet.ClearDb();

            // Add 100 guys
            for (int i = 0; i < 100; i++)
            {
                Person person = new Person();
                person.Name = $"Joe {i}";
                person.Email = $"joe{i}@example.com";
                person.Insert();
            }

            // Clear Database and reset (stop engine)
            DbDiet.ClearDb();
            DbDiet.Reset();
        }
    }
}