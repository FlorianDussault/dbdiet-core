﻿namespace DbDietCoreExamples
{
    class Program
    {
        static void Main(string[] args)
        {
            // Simple
            // Simple.Run();

            // Split objects
            // Split.Run();

            // Encrypt objects
            // Encrypt.Run();

            Chat.Run();
        }
    }
}
