﻿using System;
using System.Collections.Generic;
using System.IO;
using DbDietCore;

namespace DbDietCoreExamples
{
    /// <summary>
    /// Object Car
    /// </summary>
    [DbDietTable(FileName = nameof(Car))]
    [Serializable]
    public class Car : IDbDietObject
    {
        public uint DietId { get; set; }

        public string Model { get; set; }

        public string Brand { get; set; }

        public int Year { get; set; }
    }

    internal class Simple
    {
        /// <summary>
        /// Run !
        /// </summary>
        internal static void Run()
        {
            // Options
            DbDietOptions options = new DbDietOptions
            {
                // Database path
                DatabasePath = Path.Combine(Path.GetTempPath(), "DBDIETCORE_SIMPLE"),
                // Build directory database on start
                BuildDatabaseDirectory = true,
                // No encryption
                EncryptOptions = DbDietEncryptOptions.NoEncrypt,
                // No split
                SplitOptions = DbDietSplitOptions.NoSplit,
                // Supported objects
                SupportedObjects = new[] {typeof(Car)}
            };

            // Initialize engine
            DbDiet.Initialize(options);

            // Start engine
            DbDiet.Start();

            // Add some cars
            Car car1 = new Car {Brand = "Toyota", Model = "Yaris", Year = 2010};
            car1.Insert();
            Car car2 = new Car { Brand = "Peugeot", Model = "3008 Sport", Year = 2018 };
            car2.Insert();
            Car car3 = new Car { Brand = "Renault", Model = "Laguna", Year = 2005 };
            car3.Insert();

            // Get car
            Car peugeot = DbDiet.Get<Car>(car2.DietId);

            // Update
            peugeot.Year = 2020;
            peugeot.Update();

            // Delete
            peugeot.Delete();

            // Get all cars
            List<Car> cars = DbDiet.Get<Car>();
            Console.Write($"{cars.Count} in database");

            // Clear Database and reset (stop engine)
            DbDiet.ClearDb();
            DbDiet.Reset();
        }
    }
}
