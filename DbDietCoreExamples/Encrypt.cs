﻿using System;
using System.IO;
using DbDietCore;

namespace DbDietCoreExamples
{

    /// <summary>
    /// Person
    /// </summary>
    [DbDietTable(FileName = nameof(User), EncryptOptions = DbDietEncryptOptions.Encrypt)]
    [Serializable]
    public class User : IDbDietObject
    {
        public uint DietId { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }

    internal class Encrypt
    {
        public static void Run()
        {
            // Options
            DbDietOptions options = new DbDietOptions
            {
                // Database path
                DatabasePath = Path.Combine(Path.GetTempPath(), "DBDIETCORE_ENCRYPT"),
                // Build directory database on start
                BuildDatabaseDirectory = true,
                // Encryption keys (8 bits)
                EncryptKey = new byte[] {245, 10, 25, 56, 154, 36, 123, 6},
                EncryptIV = new byte[] {25, 210, 158, 23, 198, 230, 80, 1},
                // Supported objects
                SupportedObjects = new[] {typeof(User)}
            };

            // Initialize engine
            DbDiet.Initialize(options);

            // Start engine
            DbDiet.Start();

            // Add 100 uers
            for (int i = 0; i < 100; i++)
            {
                User user = new User();
                user.Username = $"joe_{i}";
                user.Password = $"mypassword_{i}";
                user.Insert();
            }

            // Clear Database and reset (stop engine)
            DbDiet.ClearDb();
            DbDiet.Reset();
        }
    }
}