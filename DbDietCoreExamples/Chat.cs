﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using DbDietCore;
using Microsoft.Extensions.Logging;

namespace DbDietCoreExamples
{
    /// <summary>
    /// Object User
    /// </summary>
    [DbDietTable(FileName = nameof(ChatUser))]
    [Serializable]
    public class ChatUser : IDbDietObject
    {
        public uint DietId { get; set; }

        public string Username { get; set; }

        public DateTime LastPing { get; set; }

        public void SendPing()
        {
            LastPing = DateTime.Now;
            this.Update();
        }
    }

    /// <summary>
    /// Object User
    /// </summary>
    [DbDietTable(FileName = nameof(ChatMessage))]
    [Serializable]
    public class ChatMessage : IDbDietObject
    {
        public uint DietId { get; set; }

        public uint UserId { get; set; }

        public DateTime Date { get; set; }

        public string Message { get; set; }
    }


    internal class Chat
    {
        public static void Run()
        {

            #region DbDiet

            // Options
            DbDietOptions options = new DbDietOptions
            {
                // Database path
                DatabasePath = Path.Combine(Path.GetTempPath(), "DBDIETCORE_CHAT"),
                // Build directory database on start
                BuildDatabaseDirectory = true,
                // No encryption
                EncryptOptions = DbDietEncryptOptions.NoEncrypt,
                // Split option
                SplitLimit = 10,
                // Execute by different processes
                RunningOnUniqueInstance = false,
                // Supported objects
                SupportedObjects = new[] {typeof(ChatUser), typeof(ChatMessage)}
            };

            // Logger
            options.Logger = new LoggerFactory().AddFile(Path.Combine(options.DatabasePath, "log.txt"),LogLevel.Trace).CreateLogger<DbDiet>();

            // Initialize engine
            DbDiet.Initialize(options);

            // Start engine
            DbDiet.Start();

            #endregion

            #region User registration

            Console.Write("Username: ");
            string username = Console.ReadLine();

            ChatUser currentUser = DbDiet.Get<ChatUser>().FirstOrDefault(u => string.Equals(username, u.Username, StringComparison.InvariantCultureIgnoreCase));
            if (currentUser == null)
            {
                // New user
                currentUser = new ChatUser {LastPing = DateTime.Now, Username = username};
                currentUser.Insert();
            }
            else if ((DateTime.Now - currentUser.LastPing).TotalSeconds < 20)
            {
                Console.WriteLine($"Username '{username}' is currently connected, please retry in 20 seconds");
                return;
            }

            #endregion

            Console.WriteLine("Press:");
            Console.WriteLine("\t[w] To write a message");
            Console.WriteLine("\t[q] To exit");

            uint lastMessageIndex = 0;
            while (true)
            {
                while (true)
                {
                    // Check new messages
                    List<ChatMessage> messages = DbDiet.Get<ChatMessage>();
                    if (messages.Any() && messages.Max(m=>m.DietId) > lastMessageIndex)
                    {
                        foreach (ChatMessage chatMessage in messages.Where(m => m.DietId > lastMessageIndex && m.UserId != currentUser.DietId).OrderBy(m => m.DietId))
                        {
                            // New messages !
                            ChatUser user = DbDiet.Get<ChatUser>(chatMessage.UserId);
                            Console.WriteLine($"{user?.Username ?? "Unknown"}: {chatMessage.Message}");
                            lastMessageIndex = chatMessage.DietId;
                        }
                    }

                    if (Console.KeyAvailable)
                        break;

                    Thread.Sleep(1000);
                    currentUser.SendPing();
                }

                ConsoleKeyInfo key = Console.ReadKey(true);

                if (key.KeyChar == 'q')
                {
                    // Exit
                    currentUser.LastPing = DateTime.Now.AddSeconds(-20);
                    currentUser.Update();
                    Console.WriteLine("Bye bye");
                    return;
                }
                if (key.KeyChar == 'w')
                {
                    // Write message
                    currentUser.SendPing();
                    Console.Write($"{currentUser.Username}: ");
                    string message = Console.ReadLine();
                    ChatMessage chatMessage = new ChatMessage {Date = DateTime.Now, Message = message, UserId = currentUser.DietId};
                    chatMessage.Insert();
                    currentUser.SendPing();
                }
            }
        }
    }
}
