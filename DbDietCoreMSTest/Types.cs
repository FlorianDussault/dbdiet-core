using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DbDietCore;

namespace DbDietCoreMSTest
{
    [TestClass]
    public class Types
    {
        [TestInitialize]
        public void Initialize()
        {
            Tools.DeleteDbDirectory();
        }

        [TestCleanup]
        public void Cleanup()
        {
            Tools.DeleteDbDirectory();
        }

        [TestMethod]
        public void ValidSimpleType()
        {
            DbDiet.Reset();
            Tools.DeleteDbDirectory();
            DbDiet.Initialize(new DbDietOptions()
            {
                DatabasePath = Tools.DbDirectoryFullPath,
                BuildDatabaseDirectory = true,
                SupportedObjects = new[] { typeof(FullClass) }
            });
            DbDiet.Start();

            var fullClass = new FullClass();
            fullClass.Dico = new Dictionary<string, string>();
            fullClass.Dico.Add("A","B");
            fullClass.Dico.Add("1111", "2222");
            fullClass.Byte = 222;
            fullClass.List = new List<uint>{1,2,3};
            fullClass.MixedList = new List<object>{"BONJOUR", 1, new List<char>(){'a', 'b', 'c'}};
            fullClass.SameObject = fullClass;
            fullClass.OtherObject = 32;
            fullClass.Insert();
            
            DbDiet.Reset();
            DbDiet.Initialize(new DbDietOptions()
            {
                DatabasePath = Tools.DbDirectoryFullPath,
                BuildDatabaseDirectory = true,
                SupportedObjects = new[] { typeof(FullClass) }
            });
            DbDiet.Start();
            fullClass = DbDiet.Get<FullClass>(1);
            Assert.AreEqual(2, fullClass.Dico.Count);
            Assert.IsTrue(fullClass.Dico.ContainsKey("A"));
            Assert.IsTrue(fullClass.Dico.ContainsKey("1111"));
            Assert.AreEqual(fullClass.Dico["A"], "B");
            Assert.AreEqual(fullClass.Dico["1111"], "2222");
            Assert.AreEqual((byte)222, fullClass.Byte);
            Assert.AreEqual(3, fullClass.List.Count);
            Assert.AreEqual((uint)1, fullClass.List[0]);
            Assert.AreEqual((uint)2, fullClass.List[1]);
            Assert.AreEqual((uint)3, fullClass.List[2]);

        }

        [DbDietTable(FileName = nameof(FullClass))]
        [Serializable]
        public class FullClass : IDbDietObject
        {
            public int? NullableInt1 { get; set; }
            public int? NullableInt2 { get; set; }
            public uint DietId { get; set; }
            public byte Byte { get; set; }
            public List<uint> List { get; set; }
            public Dictionary<string, string> Dico { get; set; }
            public List<object> MixedList { get; set; }
            public object OtherObject { get; set; }
            public FullClass SameObject { get; set; }
        }
    }
}