using System;
using System.IO;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DbDietCore;
namespace DbDietCoreMSTest
{
    internal static class Tools
    {
        internal static string DbDirectoryName => "UNITTEST_DB";

        internal static string DbDirectoryFullPath => Path.Combine("TEMP", DbDirectoryName);

        internal static void DeleteDbDirectory()
        {
            if (Directory.Exists(DbDirectoryFullPath))
            {
                Directory.Delete(DbDirectoryFullPath, true);
                Thread.Sleep(100);
                DeleteDbDirectory();
            }
        }

        internal static void AssertHasException(DbDietExceptionType dietExceptionType,Action action)
        {
            try
            {
                action();
                Assert.Fail("No exception !");
            }
            catch (DbDietException exception)
            {
                Assert.AreEqual(exception.DbDietExceptionType, dietExceptionType, "Wrong DbDietExceptionType");
                Assert.IsTrue(exception.Message.StartsWith("ERROR N�"), $"No message for error: {exception.DbDietExceptionType}");
            }
            catch (Exception ex)
            {
                Assert.Fail($"Not DbDietException ({ex.Message})");
            }
        }

        
    }
}
