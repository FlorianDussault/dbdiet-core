using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DbDietCore;

namespace DbDietCoreMSTest
{
    [TestClass]
    public class Generals
    {
        private DbDietOptions GoodOptions => new DbDietOptions()
        {
            DatabasePath = Tools.DbDirectoryFullPath,
            BuildDatabaseDirectory = true,
            SupportedObjects = new[] {typeof(GoodObject)}
        };

        private byte[] EncryptKey => Encoding.ASCII.GetBytes("AZERTYUI");
        private byte[] EncryptIV => Encoding.ASCII.GetBytes("AZERTYUI");

        [TestInitialize]
        public void Initialize()
        {
            Tools.DeleteDbDirectory();
        }

        [TestCleanup]
        public void Cleanup()
        {
            Tools.DeleteDbDirectory();
        }

        [TestMethod]
        public void Reset()
        {
            Tools.DeleteDbDirectory();

            if (DbDiet.HasInstance)
                DbDiet.Reset();
            Assert.IsTrue(DbDiet.HasInstance, "Instance exists");

            
            DbDiet.Initialize(GoodOptions);

            DbDiet.Start();

            Assert.IsFalse(DbDiet.HasInstance);

            DbDiet.Reset();

            Assert.IsTrue(DbDiet.HasInstance);
        }

        [TestMethod]
        public void LoadAndSave()
        {
            DbDiet.Reset();
            Tools.DeleteDbDirectory();
            DbDietOptions options = GoodOptions;
            options.SupportedObjects = new []{ typeof(GoodObject) , typeof(GoodObject2) };
            DbDiet.Initialize(options);
            DbDiet.Start();

            for (uint i = 1; i < 100; i++)
            {
                new GoodObject().Insert();
                new GoodObject2().Insert();
            }

            DbDiet.Reset();
            DbDiet.Initialize(options);
            DbDiet.Start();

            for (uint i = 1; i < 100; i++)
            {
                Assert.IsNotNull(DbDiet.Get<GoodObject>(i), $"GoodObject with id {i} is missing");
                Assert.IsNotNull(DbDiet.Get<GoodObject2>(i), $"GoodObject2 with id {i} is missing");
            }

            DbDiet.Reset();

            // Broke file
            File.WriteAllText(Path.Combine(Tools.DbDirectoryFullPath, $"{nameof(GoodObject)}.diet"), "HELLO");

            DbDiet.Initialize(options);
            Tools.AssertHasException(DbDietExceptionType.FileErrorLoad, DbDiet.Start);




        }

        [TestMethod]
        public void ClearMethods()
        {
            DbDiet.Reset();
            Tools.DeleteDbDirectory();
            DbDietOptions options = GoodOptions;
            options.SupportedObjects = new[] {typeof(GoodObject), typeof(GoodObject2)};
            DbDiet.Initialize(options);
            DbDiet.Start();

            for (uint i = 1; i < 100; i++)
            {
                new GoodObject().Insert();
                new GoodObject2().Insert();
            }

            DbDiet.ClearObjects<GoodObject>();

            Assert.IsNull(DbDiet.Get<GoodObject>(1));
            Assert.IsNotNull(DbDiet.Get<GoodObject2>(1));

            DbDiet.Reset();

            DbDiet.Initialize(options);
            DbDiet.Start();

            Assert.IsNull(DbDiet.Get<GoodObject>(1));
            Assert.IsNotNull(DbDiet.Get<GoodObject2>(1));

            DbDiet.ClearDb();

            Assert.IsNull(DbDiet.Get<GoodObject>(2));
            Assert.IsNull(DbDiet.Get<GoodObject2>(1));

            DbDiet.Reset();
            DbDiet.Initialize(options);
            DbDiet.Start();

            Assert.IsNull(DbDiet.Get<GoodObject>(2));
            Assert.IsNull(DbDiet.Get<GoodObject2>(1));
        }

        [TestMethod]
        public void Params()
        {
            Tools.DeleteDbDirectory();

            List<(DbDietExceptionType, DbDietOptions)> testList = new List<(DbDietExceptionType, DbDietOptions)>
            {
                (DbDietExceptionType.OptionsIsNull, null),
                (DbDietExceptionType.OptionsDatabasePathNullOrEmpty,
                    new DbDietOptions
                    {
                        DatabasePath = null, BuildDatabaseDirectory = true,
                        SupportedObjects = new [] {typeof(GoodObject)}
                    }),
                (DbDietExceptionType.OptionsDatabasePathNullOrEmpty,
                    new DbDietOptions
                    {
                        DatabasePath = string.Empty, BuildDatabaseDirectory = true,
                        SupportedObjects = new [] {typeof(GoodObject)}
                    }),
                (DbDietExceptionType.OptionsSupportedObjectsIsNullOrEmpty,
                    new DbDietOptions
                    {
                        DatabasePath = Tools.DbDirectoryFullPath, BuildDatabaseDirectory = true,
                        SupportedObjects = null
                    }),
                (DbDietExceptionType.OptionsSupportedObjectsIsNullOrEmpty,
                    new DbDietOptions
                    {
                        DatabasePath = Tools.DbDirectoryFullPath, BuildDatabaseDirectory = true,
                        SupportedObjects = new Type[0]
                    }),
                (DbDietExceptionType.WrongObjectDbDietTableMissing,
                    new DbDietOptions
                    {
                        DatabasePath = Tools.DbDirectoryFullPath, BuildDatabaseDirectory = true,
                        SupportedObjects = new []{typeof(DbDietTableMissing) }
                    }),
                (DbDietExceptionType.WrongObjectFilenameNullOrEmpty,
                    new DbDietOptions
                    {
                        DatabasePath = Tools.DbDirectoryFullPath, BuildDatabaseDirectory = true,
                        SupportedObjects = new []{typeof(DbDietTableFilenameMissing) }
                    }),
                (DbDietExceptionType.WrongObjectFilenameNullOrEmpty,
                    new DbDietOptions
                    {
                        DatabasePath = Tools.DbDirectoryFullPath, BuildDatabaseDirectory = true,
                        SupportedObjects = new []{typeof(DbDietTableFilenameEmpty) }
                    }),
                (DbDietExceptionType.WrongObjectFilenameNullOrEmpty,
                    new DbDietOptions
                    {
                        DatabasePath = Tools.DbDirectoryFullPath, BuildDatabaseDirectory = true,
                        SupportedObjects = new []{typeof(DbDietTableFilenameNull) }
                    }),
                (DbDietExceptionType.WrongObjectFilenameInvalid,
                    new DbDietOptions
                    {
                        DatabasePath = Tools.DbDirectoryFullPath, BuildDatabaseDirectory = true,
                        SupportedObjects = new []{typeof(DbDietTableFilenameInvalid) }
                    }),
                (DbDietExceptionType.WrongObjectIDbDietObjectMissing,
                    new DbDietOptions
                    {
                        DatabasePath = Tools.DbDirectoryFullPath, BuildDatabaseDirectory = true,
                        SupportedObjects = new []{typeof(DbDietInterfaceMissing) }
                    }),
                (DbDietExceptionType.WrongObjectNotSerializable,
                    new DbDietOptions
                    {
                        DatabasePath = Tools.DbDirectoryFullPath, BuildDatabaseDirectory = true,
                        SupportedObjects = new []{typeof(NotSerializableObject) }
                    }),
                (DbDietExceptionType.WrongObjectNull,
                    new DbDietOptions
                    {
                        DatabasePath = Tools.DbDirectoryFullPath, BuildDatabaseDirectory = true,
                        SupportedObjects = new Type[10]
                    }),
                (DbDietExceptionType.OptionsOnlyForObjects,
                    new DbDietOptions
                    {
                        DatabasePath = Tools.DbDirectoryFullPath, BuildDatabaseDirectory = true,
                        EncryptOptions = DbDietEncryptOptions.ReferToOptions, SupportedObjects = new []{typeof(GoodObject) }
                    }),
                (DbDietExceptionType.OptionsOnlyForObjects,
                    new DbDietOptions
                    {
                        DatabasePath = Tools.DbDirectoryFullPath, BuildDatabaseDirectory = true,
                        SplitOptions = DbDietSplitOptions.ReferToOptions, SupportedObjects = new []{typeof(GoodObject) }
                    }),
                (DbDietExceptionType.EncryptWrongParameters,
                    new DbDietOptions
                    {
                        DatabasePath = Tools.DbDirectoryFullPath, BuildDatabaseDirectory = true,
                        EncryptOptions = DbDietEncryptOptions.Encrypt, SupportedObjects = new []{typeof(GoodObject) },
                        EncryptKey = null, EncryptIV = EncryptIV
                    }),
                (DbDietExceptionType.EncryptWrongParameters,
                    new DbDietOptions
                    {
                        DatabasePath = Tools.DbDirectoryFullPath, BuildDatabaseDirectory = true,
                        EncryptOptions = DbDietEncryptOptions.Encrypt, SupportedObjects = new []{typeof(GoodObject) },
                        EncryptKey = new byte[0], EncryptIV = EncryptIV
                    }),
                (DbDietExceptionType.EncryptWrongParameters,
                    new DbDietOptions
                    {
                        DatabasePath = Tools.DbDirectoryFullPath, BuildDatabaseDirectory = true,
                        EncryptOptions = DbDietEncryptOptions.Encrypt, SupportedObjects = new []{typeof(GoodObject) },
                        EncryptKey = EncryptKey, EncryptIV = null
                    }),
                (DbDietExceptionType.EncryptWrongParameters,
                    new DbDietOptions
                    {
                        DatabasePath = Tools.DbDirectoryFullPath, BuildDatabaseDirectory = true,
                        EncryptOptions = DbDietEncryptOptions.Encrypt, SupportedObjects = new []{typeof(GoodObject) },
                        EncryptKey = EncryptKey, EncryptIV = new byte[0]
                    }),
                (DbDietExceptionType.EncryptWrongParameters,
                    new DbDietOptions
                    {
                        DatabasePath = Tools.DbDirectoryFullPath, BuildDatabaseDirectory = true,
                        SupportedObjects = new []{typeof(GoodObjectEncrypt) },
                        EncryptKey = null, EncryptIV = EncryptIV
                    }),
                (DbDietExceptionType.EncryptWrongParameters,
                    new DbDietOptions
                    {
                        DatabasePath = Tools.DbDirectoryFullPath, BuildDatabaseDirectory = true,
                        SupportedObjects = new []{typeof(GoodObjectEncrypt) },
                        EncryptKey = new byte[0], EncryptIV = EncryptIV
                    }),
                (DbDietExceptionType.EncryptWrongParameters,
                    new DbDietOptions
                    {
                        DatabasePath = Tools.DbDirectoryFullPath, BuildDatabaseDirectory = true,
                        SupportedObjects = new []{typeof(GoodObjectEncrypt) },
                        EncryptKey = EncryptKey, EncryptIV = null
                    }),
                (DbDietExceptionType.EncryptWrongParameters,
                    new DbDietOptions
                    {
                        DatabasePath = Tools.DbDirectoryFullPath, BuildDatabaseDirectory = true,
                        SupportedObjects = new []{typeof(GoodObjectEncrypt) },
                        EncryptKey = EncryptKey, EncryptIV = new byte[0]
                    }),
                (DbDietExceptionType.DatabaseDirectoryNotFound,
                    new DbDietOptions
                    {
                        DatabasePath = Tools.DbDirectoryFullPath, BuildDatabaseDirectory = false,
                        SupportedObjects = new []{typeof(GoodObject) }
                    })
            };

            foreach ((DbDietExceptionType, DbDietOptions) tuple in testList)
                Tools.AssertHasException(tuple.Item1, () =>
                {
                    DbDiet.Reset();
                    DbDiet.Initialize(tuple.Item2);
                    DbDiet.Start();
                });

            DbDiet.Reset();
            Tools.DeleteDbDirectory();
            DbDiet.Initialize(GoodOptions);
            Tools.DeleteDbDirectory();
            DbDiet.Reset();
            DbDietOptions options = GoodOptions;
            options.EncryptKey = EncryptKey;
            options.EncryptIV = EncryptIV;
            options.SupportedObjects = new []{typeof(GoodObjectEncrypt)};
            DbDiet.Initialize(options);
            DbDiet.Start();
        }

        [TestMethod]
        public void Starter()
        {
            Tools.DeleteDbDirectory();
            DbDiet.Reset();
            Tools.AssertHasException(DbDietExceptionType.DbNotInitialized, DbDiet.Start);

            DbDiet.Initialize(GoodOptions);

            GoodObject goodObject = new GoodObject();

            Tools.AssertHasException(DbDietExceptionType.DbNotStarted, () => DbDiet.Insert(goodObject));
            Tools.AssertHasException(DbDietExceptionType.DbNotStarted, () => DbDiet.Update(goodObject));
            Tools.AssertHasException(DbDietExceptionType.DbNotStarted, () => DbDiet.Delete(goodObject));
            Tools.AssertHasException(DbDietExceptionType.DbNotStarted, () => DbDiet.Get<GoodObject>(1));

            DbDiet.Start();

            Tools.AssertHasException(DbDietExceptionType.DbAlreadyStarted, ()=>DbDiet.Initialize(null));
            Tools.AssertHasException(DbDietExceptionType.DbAlreadyStarted, DbDiet.Start);
        }

        [TestMethod]
        public void GetMethods()
        {
            DbDiet.Reset();
            Tools.DeleteDbDirectory();
            DbDiet.Initialize(GoodOptions);
            DbDiet.Start();
            Assert.IsNull(DbDiet.Get<GoodObject>(1));
            GoodObject goodObject = new GoodObject();
            Assert.AreEqual((uint) 0, goodObject.DietId);
            goodObject.Insert();
            Assert.AreEqual((uint)1, goodObject.DietId);
            Assert.IsNotNull(DbDiet.Get<GoodObject>(1));
            Tools.AssertHasException(DbDietExceptionType.ObjectNotRegistered, ()=>DbDiet.Get<GoodObject2>(1));

            Tools.DeleteDbDirectory();
            DbDiet.Reset();
            DbDiet.Initialize(GoodOptions);
            DbDiet.Start();
            for (int i = 0; i < 10; i++)
                new GoodObject().Insert();
            Assert.AreEqual(10, DbDiet.Get<GoodObject>().Count);
            Tools.AssertHasException(DbDietExceptionType.ObjectNotRegistered, () => DbDiet.Get<GoodObject2>());
        }

        [TestMethod]
        public void UpdateMethods()
        {
            DbDiet.Reset();
            Tools.DeleteDbDirectory();
            DbDiet.Initialize(GoodOptions);
            DbDiet.Start();
            GoodObject goodObject = new GoodObject(){Year = 2005};
            goodObject.Insert();
            goodObject.Year = 2010;
            goodObject.Update();
            Assert.AreEqual(2010, goodObject.Year);
            DbDiet.Reset();
            DbDiet.Initialize(GoodOptions);
            DbDiet.Start();
            Assert.IsNotNull(DbDiet.Get<GoodObject>(goodObject.DietId));
            goodObject = DbDiet.Get<GoodObject>(goodObject.DietId);
            Assert.AreEqual(2010, goodObject.Year);
            Tools.AssertHasException(DbDietExceptionType.ObjectIsNull,()=> DbDiet.Update(default(IDbDietObject)));
            Tools.AssertHasException(DbDietExceptionType.ObjectNotRegistered, ()=>new GoodObject2{DietId = 100}.Update());
            Tools.AssertHasException(DbDietExceptionType.ObjectNotExists, () => new GoodObject {DietId = 100}.Update());
        }

        [TestMethod]
        public void InsertMethods()
        {
            DbDiet.Reset();
            Tools.DeleteDbDirectory();
            DbDiet.Initialize(GoodOptions);
            DbDiet.Start();
            GoodObject goodObject = new GoodObject { Year = 2005 };
            goodObject.Insert();
            Assert.AreEqual((uint)1, goodObject.DietId);
            Tools.AssertHasException(DbDietExceptionType.ObjectIsNull, () => DbDiet.Insert(default(IDbDietObject)));
            Tools.AssertHasException(DbDietExceptionType.ObjectNotRegistered, () => new GoodObject2 { DietId = 100 }.Insert());
        }

        [TestMethod]
        public void DeleteMethods()
        {
            DbDiet.Reset();
            Tools.DeleteDbDirectory();
            DbDiet.Initialize(GoodOptions);
            DbDiet.Start();
            GoodObject goodObject = new GoodObject { Year = 2005 };
            goodObject.Insert();
            Assert.AreEqual((uint)1, goodObject.DietId);
            goodObject.Delete();
            Assert.IsNull(DbDiet.Get<GoodObject>(goodObject.DietId));
            Tools.AssertHasException(DbDietExceptionType.ObjectIsNull, () => DbDiet.Delete(default(IDbDietObject)));
            Tools.AssertHasException(DbDietExceptionType.ObjectNotRegistered, () => new GoodObject2 { DietId = 100 }.Delete());
            Tools.AssertHasException(DbDietExceptionType.ObjectNotExists, () => new GoodObject { DietId = 100 }.Delete());
        }

        [TestMethod]
        public void Split()
        {
            List<dynamic> checkList = new List<dynamic>()
            {
                new {Max=500, Cycle=(uint)24},
                new {Max=20, Cycle=(uint)1},
                new {Max=100, Cycle=(uint)10}
            };


            foreach (dynamic toCheck in checkList)
            {
                DbDietOptions options = GoodOptions;
                options.SplitOptions = DbDietSplitOptions.Split;
                options.SplitLimit = toCheck.Cycle;

                DbDiet.Reset();
                Tools.DeleteDbDirectory();
                DbDiet.Initialize(options);
                DbDiet.Start();

                List<uint> ids = new List<uint>();
                for (int i = 0; i < toCheck.Max; i++)
                {
                    new GoodObject().Insert();
                    ids.Add((uint)i+1);
                }

                DbDiet.Reset();
                DbDiet.Initialize(options);
                DbDiet.Start();

                foreach (uint id in ids)
                {
                    Assert.IsNotNull(DbDiet.Get<GoodObject>(id));
                }


                string fileForCheckUpdate = null;

                foreach (string file in Directory.GetFiles(Tools.DbDirectoryFullPath))
                {
                    FileInfo fileInfo = new FileInfo(file);
                    List<IDbDietObject> objects = DbDiet.LoadDbObjects(file);
                    if (int.Parse(fileInfo.Name.Split('.')[1]) == 2)
                        fileForCheckUpdate = fileInfo.FullName;

                    foreach (IDbDietObject dbDietObject in objects)
                    {
                        Assert.IsTrue(ids.Contains(dbDietObject.DietId), $"Id {dbDietObject.DietId} from file {fileInfo.Name} not exists in ids");
                        ids.Remove(dbDietObject.DietId);
                    }
                }
                Assert.IsTrue(ids.Count == 0);

                if (!string.IsNullOrEmpty(fileForCheckUpdate))
                {
                    List<GoodObject> objects = DbDiet.LoadDbObjects(fileForCheckUpdate).OfType<GoodObject>().ToList();

                    foreach (GoodObject goodObject in objects)
                    {
                        goodObject.Year = 9999;
                        goodObject.Update();
                    }

                    objects = DbDiet.LoadDbObjects(fileForCheckUpdate).OfType<GoodObject>().ToList();
                    int count = objects.Count;
                    foreach (GoodObject goodObject in objects)
                    {
                        Assert.AreEqual(goodObject.Year, 9999);
                        goodObject.Delete();
                        count--;
                        List<IDbDietObject> newObjectsList = DbDiet.LoadDbObjects(fileForCheckUpdate);
                        Assert.AreEqual(count, newObjectsList.Count);
                        Assert.IsTrue(newObjectsList.All(o=>o.DietId != goodObject.DietId));
                    }
                }
            }


            List<IDbDietObject> empty = DbDiet.LoadDbObjects("xxx.diet");
            Assert.AreEqual(0, empty.Count);

            var fileWrong = Path.Combine(Tools.DbDirectoryFullPath, "test.diet");
            File.WriteAllText(fileWrong, "hello");
            Tools.AssertHasException(DbDietExceptionType.FileErrorLoad,()=>DbDiet.LoadDbObjects(fileWrong));
        }

        [TestMethod]
        public void Encrypt()
        {
            Tools.DeleteDbDirectory();
            DbDiet.Reset();
            DbDietOptions options = GoodOptions;
            options.EncryptOptions = DbDietEncryptOptions.Encrypt;
            options.EncryptKey = EncryptKey;
            options.EncryptIV = EncryptIV;
            DbDiet.Initialize(options);
            DbDiet.Start();

            GoodObject goodObject = new GoodObject{Year = 1234};
            goodObject.Insert();

            DbDiet.Reset();
            DbDiet.Initialize(options);
            DbDiet.Start();
            Assert.IsNotNull(DbDiet.Get<GoodObject>(1));
        }

        [Serializable]
        [DbDietTable(FileName = nameof(GoodObject))]
        public class GoodObject : IDbDietObject
        {
            public uint DietId { get; set; }
            public int Year { get; set; }
        }

        [Serializable]
        [DbDietTable(FileName = nameof(GoodObjectEncrypt), EncryptOptions = DbDietEncryptOptions.Encrypt)]
        public class GoodObjectEncrypt : IDbDietObject
        {
            public uint DietId { get; set; }
        }

        [Serializable]
        [DbDietTable(FileName = nameof(GoodObject2))]
        public class GoodObject2 : IDbDietObject, ICloneable
        {
            public uint DietId { get; set; }
            public string Name { get; set; }
            public object Clone()
            {
                return new GoodObject2{DietId = DietId, Name = Name};
            }
        }

        [DbDietTable(FileName = nameof(GoodObject))]
        public class NotSerializableObject : IDbDietObject
        {
            public uint DietId { get; set; }
        }

        [Serializable]
        [DbDietTable(FileName = nameof(DbDietInterfaceMissing))]
        public class DbDietInterfaceMissing
        {
            public uint DietId { get; set; }
        }

        [Serializable]
        public class DbDietTableMissing : IDbDietObject
        {
            public uint DietId { get; set; }
        }

        [Serializable]
        [DbDietTable]
        public class DbDietTableFilenameMissing : IDbDietObject
        {
            public uint DietId { get; set; }
        }

        [Serializable]
        [DbDietTable(FileName = null)]
        public class DbDietTableFilenameNull : IDbDietObject
        {
            public uint DietId { get; set; }
        }

        [Serializable]
        [DbDietTable(FileName = "")]
        public class DbDietTableFilenameEmpty : IDbDietObject
        {
            public uint DietId { get; set; }
        }

        [Serializable]
        [DbDietTable(FileName = "\\")]
        public class DbDietTableFilenameInvalid : IDbDietObject
        {
            public uint DietId { get; set; }
        }
    }
}