# DbDiet Core

[![Downloads](https://img.shields.io/nuget/dt/DbDietCore.svg?style=flat-square)](https://www.nuget.org/packages/DbDietCore/) 

DbDiet Core is a slow database engine, based on object serialization.

## Why use DbDiet Core? 👍

  - You have a small application where you have to save some objects
  - Performance is not in your interest
  - You do not need constraints
  - You are crazy

## How work DbDiet Core ?

DbDiet Core serializes objects in binary (with BinaryFormatter). It use cache in memory and has some attractive options (I think).

## Installation

Use NuGet ([DbDietCore](https://www.nuget.org/packages/DbDietCore/)) !

Packet manager:
```sh
PM> Install-Package DbDietCore -Version 1.0.2
```

.NET CLI:
```sh
> dotnet add package DbDietCore --version 1.0.2
```

Paket CLI:
```sh
> paket add DbDietCore --version 1.0.2
```

## Usage

```csharp
/// <summary>
/// Object Car
/// </summary>
[DbDietTable(FileName = nameof(Car))]
[Serializable]
public class Car : IDbDietObject
{
    // Implements by IDbDietObject
    public uint DietId { get; set; }
    
    public string Model { get; set; }
    public string Brand { get; set; }
    public int Year { get; set; }
}
```

Engine usage:
```csharp
// Options
DbDietOptions options = new DbDietOptions
{
    // Database path
    DatabasePath = Path.Combine(Path.GetTempPath(), "DBDIETCORE_SIMPLE"),
    // Build directory database on start
    BuildDatabaseDirectory = true,
    // No encryption
    EncryptOptions = DbDietEncryptOptions.NoEncrypt,
    // No split
    SplitOptions = DbDietSplitOptions.NoSplit,
    // Supported objects
    SupportedObjects = new[] {typeof(Car)}
};

// Initialize engine
DbDiet.Initialize(options);

// Start engine
DbDiet.Start();

// Add some cars
Car car1 = new Car {Brand = "Toyota", Model = "Yaris", Year = 2010};
car1.Insert();
Car car2 = new Car { Brand = "Peugeot", Model = "3008 Sport", Year = 2018 };
car2.Insert();
Car car3 = new Car { Brand = "Renault", Model = "Laguna", Year = 2005 };
car3.Insert();

// Get car
Car peugeot = DbDiet.Get<Car>(car2.DietId);

// Update
peugeot.Year = 2020;
peugeot.Update();

// Delete
peugeot.Delete();

// Get all cars
List<Car> cars = DbDiet.Get<Car>();
Console.Write($"{cars.Count} in database");
```

License
----

MIT



